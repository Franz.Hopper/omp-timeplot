#!/bin/bash

# UNUSED: Needs superuser permissions
# avgByProc() { 
#   local foo start end n=$1 e=$1 values times
#   shift; # shift all args (delete $0, the name of this script)
#   export n; # May n be available for sub-shells
  
#   { 
#     read foo;
#     read foo;
#     read foo foo start foo
#   } < /proc/timer_list;

#   mapfile values < <(
#     for((;n--;)){ "$@" &>/dev/null;}
#     read -a endstat < /proc/self/stat
#     {
#       read foo
#       read foo
#       read foo foo end foo
#     } </proc/timer_list
#     printf -v times "%s/100/$e;" ${endstat[@]:13:4}
#     bc -l <<<"$[end-start]/10^9/$e;$times"
#   )
#   printf -v fmt "%-7s: %%.5f\\n" real utime stime cutime cstime
#   printf "$fmt" ${values[@]}
# }


# Another way whitout superuser permissions
avg_time_alt() { 
    local -i c=$1 n=$2 j=0
    shift; shift;
    local foo real sys user
    unset rbest
    unset sbest
    unset ubest
    rworst=0 sworst=0 uworst=0
    ravg=0 uavg=0 savg=0

    # Check provided arguments
    (( $# > 0 )) || { echo "No binary to execute in avg_time_alt()"; exit 1;}
    if [ $c -eq 0 ] || [ $n -eq 0 ]; then
      echo "Asked for no iteration in benchmark"
      exit 1
    fi


    # For each run of a chunk
    for (( j=0; j<$n; j++ ))
    do
      { read foo real; read foo user; read foo sys;} < <(
        {
          time -p for(( ; c-- ; )) # Exec a complete chunk
          {
            "$@" &>/dev/null;
          };
        } 2>&1
      )

      rtime=$( bc -l <<< "$real/$c;" )
      utime=$( bc -l <<< "$user/$c;" )
      stime=$( bc -l <<< "$sys/$c;" )

      # Best times (smaller ones)
      if [ -z "$rbest" ] || (( $(echo "$rbest > $rtime" | bc -l) )); then
        rbest=$rtime;
      fi
      if [ -z "$ubest" ] || (( $(echo "$ubest > $utime" | bc -l) )); then
        ubest=$utime;
      fi
      if [ -z "$sbest" ] || (( $(echo "$sbest > $stime" | bc -l) )); then
        sbest=$stime;
      fi

      # Worst times (greater ones)
      if (( $(echo "$rworst < $rtime" | bc -l) )); then
        rworst=$rtime;
      fi
      if (( $(echo "$uworst < $utime" | bc -l) )); then
        uworst=$utime;
      fi
      if (( $(echo "$sworst < $stime" | bc -l) )); then
        sworst=$stime;
      fi

      # Average times
      ravg=$( bc -l <<< "$ravg + $rtime;" )
      uavg=$( bc -l <<< "$uavg + $utime;" )
      savg=$( bc -l <<< "$savg + $stime;" )
    done

    ravg=$( bc -l <<< "$ravg / $n;" )
    uavg=$( bc -l <<< "$uavg / $n;" )
    savg=$( bc -l <<< "$savg / $n;" )
}



################################################################################
# Arguments
################################################################################

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
renumber='^[0-9]+$'
tmppath="/tmp/time-plot.data"
tpath="time.png"
spath="speedup.png"
fpath="perf.png"
nbExec=10
chunksize=1
nbThreads=(2 4 8 16)
output=5
verbose=1

while getopts "c:qn:o:t:h?" opt ; do
  case "$opt" in
    h|\?)
      printf "usage: %s prog [args...]\n" $(basename $0)
      echo "Options:"
      echo "  -h or -?    Display this information."
      echo "  -c <size>   Set the size of a chunk to <size>."
      echo "  -n <runs>   Set number of runs for benchmark."
      echo "  -o <output> Output mode (0: no output, 1:table, 2:separate graphs, 4:all in one graph). Default is 5"
      echo "  -t <list>   Change the number of thread used for the benchmark."
      echo "  -q          Quiet mode."
      exit 0
      ;;

    c)
      chunksize=$OPTARG

      # Check that chunksize is an integer
      if ! [[ $chunksize =~ $renumber ]] ; then
        echo "error: chunksize must be a number" >&2;
        exit 1;
      fi
      ;;

    t)
      # Read argument as an array
      read -a nbThreads <<< $OPTARG;
      
      # Sort the array
      IFS=$'\n' nbThreads=($(sort -nu <<<"${nbThreads[*]}")); unset IFS

      # Delete bad values
      for i in ${!nbThreads[@]}
      do
        if ! [[ ${nbThreads[$i]} =~ $renumber ]] || [ ${nbThreads[$i]} -le 1 ]; then
          if [ $i -eq 0 ] && [ $i -lt $(( ${#nbThreads[@]} - 1 )) ] ; then
            nbThreads=( "${nbThreads[@]:$i+1}" )
          elif [ $i -eq $(( ${#nbThreads[@]} - 1 )) ] && [ $i -gt 0 ]; then
            nbThreads=( "${nbThreads[@]:0:$(( $i-1 ))}" )
          elif [ $i -gt 0 ] && [ $i -lt $(( ${#nbThreads[@]} - 1 )) ]; then
            nbThreads=( "${nbThreads[@]:0:$(( $i-1 ))}" "${nbThreads[@]:$i+1}" )
          elif [ ${nbThreads[$i]} -eq 1 ]; then
            nbThreads=()
          fi
          i=i-1
        fi
      done
      ;;

    n)
      nbExec=$OPTARG

      # Check that nbExec is an integer
      if ! [[ $nbExec =~ $renumber ]] ; then
        echo "error: The number of runs must be a number" >&2;
        exit 1;
      fi
      ;;

    o)
      output=$OPTARG
      # Check that output is an integer
      if ! [[ $output =~ $renumber ]] ; then
        echo "error: The output type must be a number" >&2;
        exit 1;
      fi
      ;;

    q)
      verbose=0
      ;;
  esac
done

shift "$(( $OPTIND - 1))" # So that $@ only contain what follows all options

# Exit if there is no program to call
(( $# > 0 )) || { echo "No executable has been precised"; exit 1; }
prog=$1
shift

if [ $verbose -eq 1 ]; then
  echo "Benchmark options:"
  echo "  Command being runned           :" $prog
  echo "  Command arguments              :" $@
  echo "  Thread number tested           : {1 "${nbThreads[*]}"}"
  echo "  Number of runs/chunk           :" $chunksize
  echo "  Number of chunk runned/thread  :" $nbExec
  echo "  Total number of execution      :" $(( $nbExec * $chunksize * (${#nbThreads[@]}+1))) "("$(( ${#nbThreads[@]} + 1 ))"*"$chunksize"*"$nbExec")"
  echo "  Graph saving paths             :" all_in_one: $fpath, time: $tpath, speedup: $spath
  echo
fi


################################################################################
# Initial program (with 1 thread)
################################################################################

# Creating temporary data file
echo "# Threads  Avg Real Time   Best Real Time   Worst Real Time   Avg User Time   Best User Time   Worst User Time   Avg System Time   Best System Time   Worst System Time   Speedup" > $tmppath

# Set env variable LC_NUMERIC is necessary to transfor time result from 0,9
# to a usable float for the bc command (e.g. 0.9)
export OMP_NUM_THREADS=1
if [ $verbose -eq 1 ]; then
  printf "Initial time mesurment (with 1 threads)..."
fi

LC_NUMERIC="en_US.UTF-8" avg_time_alt $chunksize $nbExec $prog $@
ref_speed=$ravg
LC_NUMERIC="en_US.UTF-8" printf "  %4d     %.6f        %.6f         %.6f          %.6f        %.6f         %.6f          %.6f          %.6f           %.6f            %.6f\n" 1 $ravg $rbest $rworst $uavg $ubest $uworst $savg $sbest $sworst 1 >> $tmppath

if [ $verbose -eq 1 ]; then
  LC_NUMERIC="en_US.UTF-8" printf "  %.6fs\n" $ravg
fi


################################################################################
# Main loop
################################################################################

for i in ${nbThreads[@]}
do
  export OMP_NUM_THREADS=$i
  if [ $verbose -eq 1 ]; then
    printf "Benchmarking with %4d threads..." $i
  fi

  LC_NUMERIC="en_US.UTF-8" avg_time_alt $chunksize $nbExec $prog $@
  speedup=$( bc -l <<< "$ref_speed/$ravg;" )
  LC_NUMERIC="en_US.UTF-8" printf "  %4d     %.6f        %.6f         %.6f          %.6f        %.6f         %.6f          %.6f          %.6f           %.6f            %.6f\n" $i $ravg $rbest $rworst $uavg $ubest $uworst $savg $sbest $sworst $speedup >> $tmppath
  
  if [ $verbose -eq 1 ]; then
    LC_NUMERIC="en_US.UTF-8" printf "           %.6fs  (%.4f speedup)\n" $ravg $speedup
  fi
done


################################################################################
# Graphical results
################################################################################


# Table
if [ $(( $output & 1 )) -eq 1 ]; then
  echo
  cat $tmppath
fi

# Separated graphs
if [ $(( $output & 2 )) -eq 2 ]; then
  gnuplot "$DIR/time.setup"
  mv -f "plot_time.png" $tpath

  gnuplot "$DIR/speedup.setup"
  mv -f "plot_speedup.png" $spath
fi

# All in one graph
if [ $(( $output & 4 )) -eq 4 ]; then
  gnuplot "$DIR/perf.setup"
  mv -f "plot_perf.png" $fpath
fi
